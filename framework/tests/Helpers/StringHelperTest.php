<?php declare(strict_types = 1);

use PHPUnit\Framework\TestCase;
use framework\src\Helpers\StringHelper;

final class StringHelperTest extends TestCase
{
    public function testSlugMakerFunctionReturnsValidSlug() : void
    {
        $this
            ->assertSame("my-bonnie-lies-over-the-ocean", StringHelper::slug("my bonnie lies over the ocean"));

        $this
            ->assertSame("سلام-بر-تو-امیر", StringHelper::slug("سلام بر تو امیر"));
    }
}
