<?php declare(strict_types = 1);

namespace framework\src\Routing;

use app\Config\AppConfig;
use app\Controllers\HomeController;

/**
 * Class Router
 * The logic for the application routing.
 */


class Router
{

    /*
     * for php 7.4
     */
    //private string $url;
    private $url;

    /*
     * controller name
     */
    private $controller;

    /*
     * method name
     */
    private $method;

    /*
     * array of parameters
     */
    private $parameters;

    /*
     * The object to be instantiated
     */
    private $controllerObject;


    public function __construct()
    {
       $this->dispatch();
    }

    /**
     * @param string $requestURI
     * @return string
     * remove the host and slashes from the url.
     */
    public function getURL(string $requestURI) : string
    {
        $url = $requestURI;
        $url = ltrim($url, "/");
        $url = trim(substr($url, strpos($url, "/")), "/");
        return $url;
    }

    public function separateURL() : void
    {
        //initialize controller, method and parameters
        // values with default values of home, index and empty array of parameters.
        $this->controller = "Home";
        $this->method = "index";
        $this->parameters = [];

        //check if controller, method and parameters exists.
        //extract the values and use them.
        if (strlen($this->url) > 0) {
            $this->url = explode("/", $this->url);

            //check for controller name.
            //if it exists, capitalize it.
            if (isset($this->url[0])) {
                $this->controller = ucwords($this->url[0]);
            }

            //check for method name
            if (isset($this->url[1])) {
                $this->method = $this->url[1];
            }

            //check for parameters
            if (isset($this->url[2])) {
                for ($i = 2;$i<sizeof($this->url);$i++) {
                    array_push($this->parameters, $this->url[$i]);
                }
            }
        }
    }


    /**
     * @return bool
     * check if the controller exists.
     */
    public function checkControllerExistence() : bool
    {
        if (is_readable(AppConfig::APP_ROOT . "/Controllers/$this->controller" . "Controller" . ".php")) {
            $className = "app\Controllers\\$this->controller" . "Controller";
            $this->controllerObject = new $className;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     * check if the method exists.
     */
    public function checkMethodExistence(): bool
    {
        if (method_exists($this->controllerObject, $this->method)) {
            return true;
        }
        return false;
    }

    /**
     * call the method from the controller.
     */
    public function callMethod() : void
    {
        call_user_func(array(
            "app\Controllers\\$this->controller" . "Controller",
            $this->method
        ), $this->parameters);
    }

    /**
     * matches the given url and calls the method inside the controller.
     */
    public function dispatch() : void
    {
        $this->url = $this->getURL($_SERVER['REQUEST_URI']);
        $this->separateURL();

        //todo throw exceptions instead of dying.
        if (!$this->checkControllerExistence()) {
            die("controller does not exist");
        }

        if (!$this->checkMethodExistence()) {
            die("method does not exist");
        }

        //todo before and after cannot be called. ensure it.
        $this->callMethod();
    }



}