<?php

namespace framework\src\Helpers;

/**
 * Class StringHelper
 * String helper functions.
 */


class StringHelper
{
    public static function slug(string $string) : string
    {
        return implode("-", explode(" ", $string));
    }
}