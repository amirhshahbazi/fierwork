<?php


namespace framework\src\Database;


class Connection extends MySQLConnection
{

    public $preparedStatement;
    public $query;

    public function __construct()
    {
        parent::__construct();
//        $this->prepare("select * from users where id = ? and age = ?");
//        $this->prepare("insert into users (name, age) VALUES (?, ?)");
//        $this->bindParameters(array(1, 25));
//        $this->bindParameters(array('amirkhan', 25));
//        $this->execute();
    }

    public function prepare(string $query) : void
    {
        $this->preparedStatement = $this->pdo->prepare($query);
    }

    public function determineParameterType($type) : int
    {
        $getType = gettype($type);
        $returnType = \PDO::PARAM_STR;
        switch ($getType) {
            case "integer":
                $returnType = \PDO::PARAM_INT;
                break;
            case "boolean":
                $returnType = \PDO::PARAM_BOOL;
                break;
        }

        return $returnType;
    }

    public function bindParameters($parameters) : void
    {
        //todo sanitize parameters.
        for ($i=0; $i<sizeof($parameters); $i++) {
            $this->preparedStatement->bindParam(
                $i+1,
                $parameters[$i],
                $this->determineParameterType($parameters[$i])
            );
        }
    }

    public function execute(string $query, array $parameters) : void
    {
        $this->prepare($query);
        $this->bindParameters($parameters);
        $this->preparedStatement->execute();
    }

    public function getResults(string $option = "") : array
    {
        $fetchMode = $option === "obj" ? \PDO::FETCH_OBJ : \PDO::FETCH_ASSOC;
        return $this->preparedStatement->fetchAll($fetchMode);
    }

    public function getOne(string $option = "")
    {
        $fetchMode = $option === "obj" ? \PDO::FETCH_OBJ : \PDO::FETCH_ASSOC;
        return $this->preparedStatement->fetch($fetchMode);
    }

}