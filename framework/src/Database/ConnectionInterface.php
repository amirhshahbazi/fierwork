<?php declare(strict_types = 1);


namespace framework\src\Database;

/**
 * Interface ConnectionInterface
 * @package framework\src\Database
 * An interface for defining sub database managers.
 */
interface ConnectionInterface
{
    public function initializeConnection() : void;
}