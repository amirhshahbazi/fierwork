<?php declare(strict_types = 1);


namespace framework\src\Database;


use app\Config\DatabaseConfig;

abstract class PostgresConnection implements ConnectionInterface
{
    /**
     * Postgres connection variable.
     */
    protected $pgConnection;

    public function initializeConnection(): void
    {
        $this->pgConnection = pg_connect(
            "host=" . DatabaseConfig::DATABASE_HOST . " dbname=" . DatabaseConfig::DATABASE_NAME
            . " user=" . DatabaseConfig::DATABASE_USER . " password=" . DatabaseConfig::DATABASE_PASSWORD
        );
    }
}