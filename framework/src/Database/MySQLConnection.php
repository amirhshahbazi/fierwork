<?php declare(strict_types = 1);

namespace framework\src\Database;

use framework\src\Database\ConnectionInterface;
use app\Config\DatabaseConfig;
use \PDO as PDO;

/**
 * Class MySQLConnection
 * @package framework\src\Database
 * Base class for MySQL Database.
 */
abstract class MySQLConnection implements ConnectionInterface
{

    /**
     * @var \PDO
     * The variable to hold pdo instance.
     */
    protected $pdo;



    public function __construct()
    {
        $this->initializeConnection();
    }

    /**
     * initialize the pdo instance with provided configuration in the DatabaseConfig file.
     */
    public function initializeConnection(): void
    {
       try {
           $this->pdo = new PDO(
               "mysql:host=" . DatabaseConfig::DATABASE_HOST . ";dbname=" . DatabaseConfig::DATABASE_NAME,
               DatabaseConfig::DATABASE_USER,
               DatabaseConfig::DATABASE_PASSWORD
           );
       } catch (\PDOException $exception) {
           die($exception);
       }
    }
}