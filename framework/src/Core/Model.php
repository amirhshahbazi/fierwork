<?php declare(strict_types = 1);

namespace framework\src\Core;


abstract class Model
{
    protected $id;
    protected $table;
}