<?php declare(strict_types = 1);


namespace framework\src\Core;


interface Controller
{
    public function index($parameters);
    public function before();
    public function after();
}