<?php declare(strict_types = 1);

use framework\src\Routing\Router;
use framework\src\Helpers\StringHelper;
use app\Config\AppConfig;

/**
 * Entry point for the application.
 * all the routing happens here.
 */


require '../vendor/autoload.php';


$a = new Router();

