<?php declare(strict_types = 1);

namespace app\Repositories;


interface BaseRepository
{
    public function getById();
    public function getAll();
    public function insertOne();
    public function insertBatch();
}