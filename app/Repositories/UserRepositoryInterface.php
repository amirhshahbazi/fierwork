<?php


namespace app\Repositories;


interface UserRepositoryInterface extends BaseRepository
{
    public function getAuthenticatedUser();
}