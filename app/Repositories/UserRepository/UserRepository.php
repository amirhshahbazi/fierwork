<?php


namespace app\Repositories\UserRepository;


use app\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    public function getById()
    {
    }

    public function getAll()
    {
    }

    public function insertOne()
    {
    }

    public function insertBatch()
    {
    }

    public function getAuthenticatedUser()
    {
    }
}