<?php declare(strict_types = 1);

namespace app\Config;

/**
 * Class DatabaseConfig
 * Defining database configuration parameters.
 * might use .env later.
 */


class DatabaseConfig
{
//    const DATABASE_HOST = "database host, e.g. mysql";
//    const DATABASE_NAME = "database name, e.g. ecommerce";
//    const DATABASE_USER = "database user";
//    const DATABASE_PASSWORD = 'database password';

    const DATABASE_HOST = "localhost";
    const DATABASE_NAME = "framework_test";
    const DATABASE_USER = "framework_user";
    const DATABASE_PASSWORD = 'framework_password';

}