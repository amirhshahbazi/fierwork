# PHP MVC Framework FierWork (Fierce Framework)

A lightweight, minimal MVC framework for developing PHP applications.



### How to install
1.  clone or download the repository.
2.  run composer install.

### Configuration
1. change the database configuration in **app/config/DatabaseConfig.php**
2. change the application root directory in **app/config/AppConfig.php**

### Models
every model class must be defined in **app/Models** and extend Model abstract class from **framework/src/Core/Model.php**



